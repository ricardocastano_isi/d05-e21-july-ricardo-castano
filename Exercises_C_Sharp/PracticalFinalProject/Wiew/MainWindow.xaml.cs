﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model;
using BLL;
using System.IO;

namespace Wiew
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Btn_OkLogin_Click(object sender, RoutedEventArgs e)
        {
            if ((Txtbox_UserNameLogin.Text == "") || (Txtbox_UsePassword.Text.ToString() == "")){
                MessageBox.Show("Please enter data");
            }
            else
            {
                Account aco = new Account();
                aco = AccountManager.LookForAccount(long.Parse(Txtbox_UserNameLogin.Text), Txtbox_UsePassword.Text.ToString());
                if (aco!=null)
                {
                    TransactionWindow tran = new TransactionWindow();
                    tran.Show();
                    tran.TxtBox_AcountNumber.Text = "#"+Txtbox_UserNameLogin.Text;
                    tran.TxtBox_Balance.Text = "$"+aco.Balance.ToString();
                    
                    this.Close();
                }
                else
                {
                    MessageBox.Show("User or Password incorrect, please validate data");
                    Txtbox_UserNameLogin.Text = "";
                    Txtbox_UsePassword.Text = "";
                }
              
            }


        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            Account ac = new Account() { Number = 123456, Password = "abc123…", Balance = 880 };
            Account ac2 = new Account() { Number = 41134,  Password = "toto007…", Balance = 7820 };
            if (File.Exists("../../accounts.txt"))
                { File.Delete("../../accounts.txt"); }
            AccountManager.SaveAccount(ac);
            AccountManager.SaveAccount(ac2);
        }
    }
}
