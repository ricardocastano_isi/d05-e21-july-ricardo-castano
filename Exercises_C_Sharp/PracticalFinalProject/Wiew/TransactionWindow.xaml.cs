﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Wiew
{
    /// <summary>
    /// Interaction logic for TransactionWindow.xaml
    /// </summary>
    public partial class TransactionWindow : Window
    {
        public TransactionWindow()
        {
            InitializeComponent();
        }

        private void Btn_Logout_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Btn_Deposit_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Btn_Withdraw_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
