﻿using DAL;
using System;
using System.Collections.Generic;
using BLL;
using Model;

namespace BLL
{
    public static class AccountManager
    {
        public static Account LookForAccount(long aco,string pw )
        {
            return AccountService.ValidateUserAccount(aco, pw);
        }
        public static bool DepositForAccount(long aco, double amo)
        {
            return AccountService.DepositAccount( aco, amo);
        }
        public static bool WithdrawForAccount(long aco, double amo)
        {
            return AccountService.WithdrawAccount(aco, amo);
        }
        public static bool SetAccountByNumber(long ac,double am)
        {
            return AccountService.SetAccountByNumber(ac, am);
        }
        public static void SaveAccount(Account newAcount)
        {
            AccountService.SaveAccountToDataStorage(newAcount);

        }
    }
}
