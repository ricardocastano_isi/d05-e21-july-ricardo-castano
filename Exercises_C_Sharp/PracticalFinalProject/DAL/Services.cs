﻿using Model;
using System.Collections.Generic;
using System.IO;

namespace DAL
{
    public static class AccountService
    {
        public static string filePath = "../../accounts.txt";
        public static void SaveAccountToDataStorage(Account acAdd)
        {
            string str = string.Format("{0}/{1}/{2}\n", acAdd.Number, acAdd.Password, acAdd.Balance);
            File.AppendAllText(filePath, str);
        }

        public static bool SetAccountByNumber(long ac, double am)
        {
            bool validate = false;
            string[] lines = File.ReadAllLines(filePath);
            string[] newLines = new string[lines.Length];
            int j = 0;
            foreach (string line in lines)
            {
                string[] data = line.Split('/');
                if (int.Parse(data[0]) == ac)
                {
                    newLines[j] = string.Format("{0}/{1}/{2}", data[0], data[1], data[2]);
                    validate = true;
                }
                else
                {
                    newLines[j] = string.Format("{0}/{1}/{2}", data[0], data[1], data[2]);
                }
                j++;
            }
            File.WriteAllLines(filePath, newLines);
            return validate;
        }
        public static Account ValidateUserAccount(long ac, string pw)
        {
            Account foundAccount = null;
            string[] lines = File.ReadAllLines(filePath);
            foreach (string line in lines)
            {
                string[] data = line.Split('/');
                if ((long.Parse(data[0])) == ac && (data[1]==pw))
                {
                    foundAccount = new Account
                    {
                        Number = long.Parse(data[0]),
                        Password = data[1],
                        Balance = double.Parse(data[2])
                    };
                    break;
                }
            }
             return foundAccount;
        }
        public static bool DepositAccount(long aco, double amo)
        {
            bool validate = false;
            string[] lines = File.ReadAllLines(filePath);
            string[] newLines = new string[lines.Length];
            int j = 0;
            foreach (string line in lines)
            {
                string[] data = line.Split('/');
                if (int.Parse(data[0]) == aco )
                {
                    double Bal = double.Parse(data[2]) + amo;
                    newLines[j] = string.Format("{0}/{1}/{2}", data[0], data[1], Bal);
                    validate = true;
                }
                else
                {
                    newLines[j] = string.Format("{0}/{1}/{2}", data[0], data[1], data[2]);
                }
                j++;
            }
            File.WriteAllLines(filePath, newLines);
            return validate;
        }
        public static bool WithdrawAccount(long aco, double amo)
        {
            bool validate = false;
            string[] lines = File.ReadAllLines(filePath);
            string[] newLines = new string[lines.Length];
            int j = 0;
            foreach (string line in lines)
            {
                string[] data = line.Split('/');
                if (int.Parse(data[0]) == aco)
                {
                    double Bal = double.Parse(data[2]) - amo;
                    if (Bal > 0){
                        newLines[j] = string.Format("{0}/{1}/{2}", data[0], data[1],Bal );
                        validate = true;
                    }
                  
                }
                else
                {
                    newLines[j] = string.Format("{0}/{1}/{2}", data[0], data[1], data[2]);
                }
                j++;
            }
            File.WriteAllLines(filePath, newLines);
            return validate;
        }
    }
}
