﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    
    public class Account
    {
        public long Number { get; set; }
        public string Password { get; set; }
        public double Balance { get; set; }

        public event EventHandler balanceChange;

        public void BalanceChange()
        {
            if (balanceChange != null)
            {
                balanceChange(this, EventArgs.Empty);
            }
        }

        public override string ToString()
        {
            return string.Format("Account Number #: {0},  Password: {2}, Balance: {3} "
                +"", this.Number,this.Password ,this.Balance);
        }

        public void Deposit(double amount)
        {
            this.Balance += amount;
        }
        public void Withdraw(double amount)
        {
            this.Balance -= amount;
        }

    }
    class Owner
    {
        
        public Account Account { get; set; }

        public Owner(Account account)
        {
            this.Account = account;
            
            this.Account.balanceChange += HostAlarmOnHandler;
        }

     
        public void HostAlarmOnHandler(object sender, EventArgs e)
        {
            Console.WriteLine("thieves caught by owner, thanks {0}", ((Account)sender).Balance);
        }
    }
}
