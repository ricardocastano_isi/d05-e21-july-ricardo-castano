﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _03__Exercises
{
    namespace Utilities
    {
        enum dataType { INT, DOUBLE, FLOAT, NONE };
        class validators
        {
            public static bool ValidateInput(string input, dataType type = dataType.NONE)
            {
                bool success = false;
                switch (type)
                {
                    case dataType.INT:
                        success = int.TryParse(input, out int numInt); break;
                    case dataType.DOUBLE:
                        success = double.TryParse(input, out double numDouble); break;
                    case dataType.FLOAT:
                        success = float.TryParse(input, out float numFloat); break;
                    default:
                        Console.WriteLine("Number type not matches."); break;
                }
                if (!success)
                    Console.WriteLine($"Attempted conversion of '{input ?? "<null>"}' to " + type + " failed.");
                return success;
            }

            public static bool ValidateInput(int num, int min, int max)
            {
                bool success = true;
                if (num < min || num > max)
                {
                    success = false;
                    Console.WriteLine("Number ({0}) out of range, must be between {1} and {2} ", num, min, max);
                }
                return success;
            }
        }
    }
    namespace Arrays
    {
        class _Array
        {
            public static int maxNumArray;

            public int GetMaxNumArray()
            {
                return maxNumArray;
            }
            public void SetMaxNumArray(int first, int second)
            {
                //var Result=first > second ? maxNum = first : maxNum = second;
                if (first > second) maxNumArray = first;
                else maxNumArray = second;
            }

            public int[] Swap(int[] array, bool ascending = true)
            {
                int tmp;
                for (int j = 0; j <= array.Length - 2; j++)
                {
                    for (int i = 0; i <= array.Length - 2; i++)
                    {
                        if (ascending is true)
                        {
                            if (array[i] > array[i + 1])
                            {
                                tmp = array[i + 1];
                                array[i + 1] = array[i];
                                array[i] = tmp;
                            }
                        }
                        else
                        {
                            if (array[i] < array[i + 1])
                            {
                                tmp = array[i];
                                array[i] = array[i + 1];
                                array[i + 1] = tmp;
                            }

                        }
                    }
                }
                return array;
            }
            public void PrintArray(int[] array, bool withIndex = false)
            {
                String _elements = "";
                foreach (int a in array)
                {
                    if (withIndex is true)
                        _elements += ("{" + (Array.IndexOf(array, a)).ToString() + "}=" + a + " ");
                    else
                        _elements += a + " ";
                }
                Console.WriteLine("[ " + _elements + "]");
            }
            public int[] AddArray()
            {
                int[] array = new int[0];
                Console.WriteLine("Please enter array length: ");
                String input = Console.ReadLine();
                if (Utilities.validators.ValidateInput(input, Utilities.dataType.INT))
                {
                    array = new int[Int32.Parse(input)];
                    for (int i = 0; i < array.Length; i++)
                    {
                        do
                        {
                            Console.Write("Please enter index[{0}] : ", i);
                            input = Console.ReadLine();
                        } while (!Utilities.validators.ValidateInput(input, Utilities.dataType.INT));
                        array[i] = Int32.Parse(input);
                    }
                }
                return array;
            }
            public void TestingClassArray()
            {
                int[] array = AddArray();
                if (array.Length > 0)
                    PrintArray(array);
            }
        }
    }
    namespace GetMax_Three_Numbers
    {
        class Program
        {
            public static void Question1()
            {
                /*1-Create a method GetMax() with two integer (int) parameters, that returns maximal of the two numbers. 
                 * Write a program that reads three numbers from the console and prints the biggest of them. Use the GetMax()
                 * method you just created. Write a test program that validates that the methods works correctly.*/
                try
                {
                    Dictionary<string, int> numbers = new Dictionary<string, int>();
                    for (int i = 1; i < 4; i++)
                    {
                        Console.WriteLine("Please enter {0} number: ", i);
                        int input = Int32.Parse(Console.ReadLine());
                        numbers.Add(String.Format("number_{0}", i.ToString()), input);
                    }
                    Arrays._Array objMax = new Arrays._Array();
                    objMax.SetMaxNumArray(numbers["number_1"], numbers["number_2"]);
                    objMax.SetMaxNumArray(numbers["number_3"], objMax.GetMaxNumArray());
                    Console.WriteLine("The biggest number beetween {0}, {1} and {2} is: {3}" +
                        "", numbers["number_1"], numbers["number_2"], numbers["number_3"], objMax.GetMaxNumArray());
                    /*foreach (var item in numbers)
                    {
                        Console.Write(" key "+item.Key);
                        Console.Write(" value "+item.Value);
                    }*/
                }
                catch (Exception e)
                {
                    Console.WriteLine("Something went wrong. Error:" + e);
                }
            }
        }
    }
    namespace English_Name_Last_Digit
    {
        class Program
        {
            static void GetNameEnglish(string number)
            {
                switch (number[number.Length - 1])
                {
                    case '1':
                        Console.WriteLine("One"); break;
                    case '2':
                        Console.WriteLine("Two"); break;
                    case '3':
                        Console.WriteLine("Three"); break;
                    case '4':
                        Console.WriteLine("Four"); break;
                    case '5':
                        Console.WriteLine("Five"); break;
                    case '6':
                        Console.WriteLine("Six"); break;
                    case '7':
                        Console.WriteLine("Seven"); break;
                    case '8':
                        Console.WriteLine("Eight"); break;
                    case '9':
                        Console.WriteLine("Nine"); break;
                    case '0':
                        Console.WriteLine("Zero"); break;
                    default:
                        Console.WriteLine("Not inputted number."); break;
                }
            }
            public static void Question1()
            {
                /*2-Write a method that returns the English name of the last digit of a given number. 
                 * Example: for 512 prints "two"; for 1024 to "four".*/
                try
                {
                    Console.Write("Please Enter a number: ");
                    String input = Console.ReadLine();
                    if ((Utilities.validators.ValidateInput(input, Utilities.dataType.DOUBLE))
                        || (Utilities.validators.ValidateInput(input, Utilities.dataType.INT))
                        || (Utilities.validators.ValidateInput(input, Utilities.dataType.FLOAT)))
                    {
                        GetNameEnglish(input);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Something went wrong. Error:" + e);
                }
            }
        }
    }

    namespace Find_Number_Into_Matrix
    {
        class Program
        {
            static int CountNumberArray(int[] array, int number)
            {
                int counter = 0;
                for (int i = 0; i < array.Length; i++)
                {
                    if (number == array[i]) counter++;
                }
                return counter;
            }
            public static void Question1()
            {
                /*3-Write a method that finds how many times certain number can be found in a given array. 
                 * Write a program to test that the method works correctly.*/
                try
                {
                    Arrays._Array objFindNum = new Arrays._Array();
                    int[] array = objFindNum.AddArray();
                    if (array.Length > 0)
                    {
                        Console.Write("Please enter number to find: ");
                        String input = Console.ReadLine();
                        if (Utilities.validators.ValidateInput(input, Utilities.dataType.INT))
                        {
                            int num_Find = Int32.Parse(input);
                            Console.WriteLine("{0} was found {1} times.", num_Find, CountNumberArray(array, num_Find));
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Something went wrong. Error:" + e);
                }
            }

        }
    }
    namespace Checks_Elements_Arrays
    {
        class Program
        {
            static void GetGreater(int index, int neighbor, string location)
            {
                if (index > neighbor) Console.WriteLine("{0} is bigger " +
                   " than it's " + location + " {1}.", index, neighbor);
                else Console.WriteLine("{0} is smaller than it's " + location + " {1}.", index, neighbor);
            }

            static void CheckElement_Position(int[] array, int number)
            {
                if (array.Length == 2)
                {
                    if (number == 0)
                        GetGreater(array[number], array[number + 1], "right");
                    else
                        GetGreater(array[number], array[number - 1], "left");
                }
                else if (array.Length >= 3)
                {
                    if (number == 0)
                        GetGreater(array[number], array[number + 1], "right");
                    else if (number == array.Length - 1)
                        GetGreater(array[number], array[number - 1], "left");
                    else
                    {
                        GetGreater(array[number], array[number + 1], "right");
                        GetGreater(array[number], array[number - 1], "left");
                    }
                }
            }
            public static void Question1()
            {
                /*4-Write a method that checks whether an element, from a certain position in an array is greater
                 * than its two neighbors. Test whether the method works correctly..*/
                try
                {
                    Arrays._Array objCheckNum = new Arrays._Array();
                    int[] array = objCheckNum.AddArray();
                    if (array.Length > 0)
                    {
                        objCheckNum.PrintArray(array, true);
                        if (array.Length == 1)
                            Console.WriteLine("The array only has one element, Lenght={0}", array.Length);
                        else
                        {
                            Console.Write("Please enter index to validate: ");
                            String input = Console.ReadLine();
                            if (Utilities.validators.ValidateInput(input, Utilities.dataType.INT))
                            {
                                if (Utilities.validators.ValidateInput(Int32.Parse(input), 0, array.Length - 1))
                                    CheckElement_Position(array, Int32.Parse(input));
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Something went wrong. Error:" + e);
                }
            }

        }
    }

    namespace Find_Biggest_Element_Array
    {
        class Program
        {
            public static void Question1()
            {
                /*5-Write a method that finds the biggest element of an array. 
                 * Use that method to implement sorting in ascending order.*/
                try
                {
                    Arrays._Array objSortNum = new Arrays._Array();
                    int[] array = objSortNum.AddArray();
                    if (array.Length > 0)
                        objSortNum.PrintArray(array, true);
                    if (array.Length == 1)
                        Console.WriteLine("The array only has one element, Lenght={0}", array.Length);
                    else
                    {
                        int[] arraySort = objSortNum.Swap(array);
                        objSortNum.PrintArray(arraySort);
                        Console.WriteLine("{0} is the biggest element of the array.", arraySort[arraySort.Length - 1]);
                    }
                    /*for (int i = 0; i < array.Length - 1; i++)
                         objSortNum.SetMaxNumArray(array[i], array[i + 1]);
                    Console.WriteLine("{0} is the biggest element of the array.", objSortNum.GetMaxNumArray());*/

                }
                catch (Exception e)
                {
                    Console.WriteLine("Something went wrong. Error:" + e);
                }
                Console.ReadLine();
            }
        }
    }

    namespace Min_Max_Array
    {
        class Program
        {
            public static void MinMaxArray(float[] data, ref float min, ref float max)
            {
                min = data[1];
                max = data[2];
            }
            public static void Question1()
            {
                /*6-Write a function named MinMaxArray, to return the minimum and maximum values stored in an array,
                 using reference parameters:
                float[] data = { 1.5f, 0.7f, 8.0f }  MinMaxArray(data, ref minimum, ref maximum);
                (after that call, minimum would contain 0.7, and maximum would contain 8.0)*/
                float[] data = { 1.5f, 0.7f, 8.0f };
                float minimum = 0.0f;
                float maximum = 0.0f;
                foreach (var item in data)
                    Console.WriteLine(" " + item.ToString());
                Console.WriteLine(" Variables before, Minimum= {0} and Maximum= {1}", minimum, maximum);
                MinMaxArray(data, ref minimum, ref maximum);
                Console.WriteLine(" Variables after, Minimum= {0} and Maximum= {1}", minimum, maximum);
            }
        }
    }

    namespace Calculate_Double_Integer_Number
    {
        class Calculator
        {
            public void Double(int n, out int m)
            {
                m = n * n;
            }
        }
        class Program
        {
            public static void Question1()
            {
                /*Write a method named "Double" to calculate the double of an integer number, and modify the data passed
                 * as an argument.It must be a "void" function and you must use the out parameter modifier.*/
                Console.WriteLine("Please enter array length: ");
                String input = Console.ReadLine();
                if (Utilities.validators.ValidateInput(input, Utilities.dataType.INT))
                {
                    int numero = int.Parse(input), m = 0;
                    new Calculator().Double(numero, out m);
                    Console.WriteLine("Result: {0}", m);
                }
            }
        }
    }

    namespace Sum_All_Numbers
    {
        class Program
        {
            static int Add(params int[] nums)
            {
                int counter = 0;
                for (int i = 0; i < nums.Length; i++)
                {
                    Console.WriteLine(nums[i]);
                    counter += nums[i];
                }
                return counter;
            }
            public static void Question1()
            {
                /*Write a method named "Add" that will receives any number of integer parameters at run
                 * time and returns the sum of all those number. You should use the params modifier*/

                int[] array = new int[3] { 10, 20, 40 };
                Console.WriteLine("Sum of the total numbers: {0}", Add(array));
            }
        }
    }
    class Program
    {
        private static bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Choose an option:");
            Console.WriteLine("1) Testing Class Array");
            Console.WriteLine("2) Exercise GetMax with three integer (int) parameters");
            Console.WriteLine("3) Exercise english name of the last digit of a given number");
            Console.WriteLine("4) Exercise how many times certain number found in a array");
            Console.WriteLine("5) Exercise checks elements neighbors of arrays");
            Console.WriteLine("6) Exercise finds the biggest element of an array");
            Console.WriteLine("7) Exercise minimum and maximum using reference parameters");
            Console.WriteLine("8) Exercise calculate the double of an integer number");
            Console.WriteLine("9) Exercise calculate sum of several numbers");
            Console.WriteLine("10) Exit");
            Console.Write("\r\nSelect an option: ");
            switch (Console.ReadLine())
            {
                case "1":
                    Console.Clear();
                    Arrays._Array objArray = new Arrays._Array();
                    objArray.TestingClassArray();
                    Console.ReadLine();
                    return true;
                case "2":
                    Console.Clear();
                    GetMax_Three_Numbers.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "3":
                    Console.Clear();
                    English_Name_Last_Digit.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "4":
                    Console.Clear();
                    Find_Number_Into_Matrix.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "5":
                    Console.Clear();
                    Checks_Elements_Arrays.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "6":
                    Console.Clear();
                    Find_Biggest_Element_Array.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "7":
                    Console.Clear();
                    Min_Max_Array.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "8":
                    Console.Clear();
                    Calculate_Double_Integer_Number.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "9":
                    Console.Clear();
                    Sum_All_Numbers.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "10":
                    Console.WriteLine("Program end...");
                    Thread.Sleep(2000);
                    return false;
                default:
                    return true;
            }

        }
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            bool showMenu = true;
            while (showMenu)
            {
                showMenu = MainMenu();
            }
        }

    }
}
