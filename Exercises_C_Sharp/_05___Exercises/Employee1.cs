﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _05___Exercises
{
    public partial class Employee
    {
        int Id;
        String FirstName;
        String LastName;
        DateTime Birthday;
        int SocialInsuranceNum;

        public Employee(int id,string firstName, string lastName,DateTime birthDay, int numSocial)
        {
            this.Id = id;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Birthday = birthDay;
            this.SocialInsuranceNum = numSocial;
        }
        ~Employee()
        {
            Console.WriteLine("destroying EmployeeTest object...");
            Thread.Sleep(2000);
        }
    }
}
