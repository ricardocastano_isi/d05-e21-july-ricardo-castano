﻿//Developed by Ricardo Andres Castano Bustos
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _05___Exercises
{
    public enum Universities
    {
        Montreal, Toronto, Andes, MIT, Oxford, Harward, NONE
    }
    public enum Subjects
    {
        Programming, Logic, mathematics, Business, NONE
    }
    public class Student
    {
        static String fullName;
        static int course= default(int);
        static Subjects subject;
        static Universities university;
        static String email;
        static long? phoneNumber;
        static int counterObjects=0;

        public string FullName
        {
            get { return fullName; }
            set { fullName = value;}
        }
        public int Course
        {
            get { return course; }
            set { course = value; }
        }
        public Subjects Subject
        {
            get { return subject; }
            set { subject = value; }
        }
        public Universities University
        {
            get { return university; }
            set { university = value; }
        }
        public String Email
        {
            get { return email; }
            set { email = value; }
        }
        public long? PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }

        ~Student()
        {
            Console.WriteLine("destroying Student object...");
            Thread.Sleep(2000);
        }
        public Student(string name, int course, Subjects subject, Universities university, string email, long? phone = null)
        {
            this.FullName = name;
            this.Course = course;
            this.Subject = subject;
            this.University = university;
            this.Email = email;
            this.PhoneNumber = phone;
            counterObjects += 1;
        }

        public Student()
        : this("No name")
        {
        }
        public Student(string name)
            : this(name, -9999)
        {
        }
        public Student(string name, int course)
            : this(name, course,Subjects.NONE)
        {
        }
        public Student(string name, int course, Subjects subject)
            : this(name, course, subject, Universities.NONE)
        {
        }
        public Student(string name, int course, Subjects subject, Universities university,string email)
           : this(name, course, subject, university, email,null)
        {
        }
        public Student(string name, int course, Subjects subject, Universities university)
            : this(name, course, subject, university, "__________@gmail.com", null)
        {
        }
        public void GetDataStudents()
        {
            Console.WriteLine(" Full Name: {0}\n Course: {1}\n Subject: {2}\n University: {3}\n E-mail: {4}\n Phone: {5}\n Total Objects: {6}" +
                "",fullName,course,subject,university,email,phoneNumber,counterObjects+"\n");
        }
    }
    public class StudentTest
    {
        //static Student stu1,stu2,stu3,stu4,stu5,stu6,stu7 = new Student();
        static Student[] students = new Student[8];
        public static void CreateStudents()
        {
            students[0] = new Student();
            students[0].GetDataStudents();
            students[1]= new Student("Felipe Bustos");
            students[1].GetDataStudents();
            students[2] = new Student("Felipe Bustos");
            students[2].GetDataStudents();
            students[3] = new Student("Pepito Perez", 505);
            students[3].GetDataStudents();
            students[4] = new Student("Andrew Camilo",808, Subjects.Business);
            students[4].GetDataStudents();
            students[5] = new Student("Fanny Bustos", 810, Subjects.mathematics, Universities.Montreal);
            students[5].GetDataStudents();
            students[6] = new Student("Camilo Pacheco", 701, Subjects.Logic, Universities.MIT, "camilo@gmail.com");
            students[6].GetDataStudents();
            students[7] = new Student("Andres Castano", 404, Subjects.Programming, Universities.MIT, "ac@gmail.com",154187787454);
            students[7].GetDataStudents();
        }
    }

    public class Book
    {
        public String Title { get; set; }
        public String Author { get; set; }
        public String Publisher { get; set; }
        public DateTime ReleaseDate { get; set; }
        public String ISBNnumber { get; set; }

        ~Book()
        {
            Console.WriteLine("destroying Book object...");
            Thread.Sleep(2000);
        }
    }
    public class Library
    {
        public String LibraryName { get; set; }
        public List<Book> Books = new List<Book>();

        ~Library()
        {
            Console.WriteLine("destroying Library object...");
            Thread.Sleep(2000);
        }
        public void AddBook(String libraryName)
        {
            this.LibraryName = libraryName;
        }

        public void AddBook(string title, string author, string publisher, DateTime releaseDate, string iSBNnumber)
        {
            Book newBook = new Book
            {
                Author = author,
                Title = title,
                ISBNnumber = iSBNnumber,
                Publisher = publisher,
                ReleaseDate = releaseDate
            };
            Books.Add(newBook);
        }

        public void GetDataLibrary(Book book)
        {
            Console.WriteLine("Title: {0},  Author: {1},  ISBNnumber: {2}", book.Title, book.Author, book.ISBNnumber);
        }
        public void DeleteBook(Book book)
        {
            Books.Remove(book);
        }
        public List<Book> SearchBook(String author)
        {
            //var authorName = "Stephen King";
            return Books.Where(x => x.Author == author).ToList();
        }

    }
    public class LibraryTest
    {
        public static void CreateLibrary()
        {
            Library lib = new Library();
            lib.AddBook("Public Library of Montreal Canada\n");
            lib.AddBook("C#", "Anubis", "Norma", DateTime.Now, "ISBN000002");
            lib.AddBook("C++", "kansadosl", "Norma", DateTime.Now, "ISB000004");
            lib.AddBook("JavaScript", "Gabriel Garcia", "Casanova", DateTime.Now, "ISBN00005");
            lib.AddBook("Bussines", "Jose Rulfo", "Anonen", DateTime.Now, "ISBN00006");
            lib.AddBook("College", "Stephen King", "Alie", DateTime.Now, "ISBN00007");
            lib.AddBook("Time", "Stephen King", "Ann", DateTime.Now, "ISBN00008");
            lib.AddBook("Other", "Stephen King", "nen", DateTime.Now, "ISBN00009");
            Console.WriteLine("\nTotal books of the library: {0}", lib.LibraryName);
            if (lib.Books.Count > 0)
            {
                foreach (var book in lib.Books)
                {
                    lib.GetDataLibrary(book);
                }

                Console.WriteLine("\nDeleting books by Stephen King");
                var seachedBooks = lib.SearchBook("Stephen King");
                Console.WriteLine(seachedBooks.Count + " books deleted");
                foreach (var book in seachedBooks)
                {
                    lib.DeleteBook(book);
                }
                Console.WriteLine("\nbooks in Library of: {0}", lib.LibraryName);
                foreach (var book in lib.Books)
                {
                    lib.GetDataLibrary(book);
                }
            }
            //Second Library
            Library lib2 = new Library();
            lib2.AddBook("Private Library of New College\n");
            lib2.AddBook("Social I", "Cesar Cuyabro", "Norma", DateTime.Now, "ISBN000002");
            lib2.AddBook("Tupla II", "Kin Son One", "Norma", DateTime.Now, "ISB000004");
            Console.WriteLine("\nTotal books of the library: {0}", lib2.LibraryName);
            if (lib2.Books.Count > 0)
            {
                foreach (var book in lib2.Books)
                {
                    lib2.GetDataLibrary(book);
                }
                Console.WriteLine("\nDeleting books by Kin Son One");
                var seachedBooks = lib2.SearchBook("Kin Son One");
                Console.WriteLine(seachedBooks.Count + " books deleted");
                foreach (var book in seachedBooks)
                {
                    lib2.DeleteBook(book);
                }
                Console.WriteLine("\nbooks in Library of: {0}", lib2.LibraryName);
                foreach (var book in lib2.Books)
                {
                    lib2.GetDataLibrary(book);
                }
            }
        }
    }

    public class EmployeeTest
    {
        ~EmployeeTest()
        {
            Console.WriteLine("destroying EmployeeTest object...");
            Thread.Sleep(2000);
        }
        public static void CreateEmployee()
        {
            Employee emp = new Employee(1,"Andres", "Castano Bustos", new DateTime(1986,03,17), 152578);
            emp.GetEmployee();
        }
    }
    class Program
    {
        private static bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Choose an option:");
            Console.WriteLine("1) Exercise Student Class");
            Console.WriteLine("2) Exercise Book library");
            Console.WriteLine("3) Exercise Partial class");
            Console.WriteLine("4) Exit");
            Console.Write("\r\nSelect an option: ");
            switch (Console.ReadLine())
            {
                case "1":
                    Console.Clear();
                    StudentTest.CreateStudents();                 
                    Console.ReadLine();
                    return true;
                case "2":
                    Console.Clear();
                    LibraryTest.CreateLibrary();
                    Console.ReadLine();
                    return true;
                case "3":
                    Console.Clear();
                    EmployeeTest.CreateEmployee();
                    Console.ReadLine();
                    return true;
                case "4":
                    Console.WriteLine("Program end...");
                    Thread.Sleep(2000);
                    return false;
                default:
                    return true;
            }

        }
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            bool showMenu = true;
            while (showMenu)
            {
                showMenu = MainMenu();
            }
        }

    }
}
//Developed by Ricardo Andres Castano Bustos
