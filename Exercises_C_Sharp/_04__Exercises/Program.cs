﻿//Developed by Ricardo Andres Castano Bustos
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _04__Exercises
{
    namespace Utilities
    {
        enum DataType { INT, DOUBLE, FLOAT, NONE };
        class validators
        {
            public static bool ValidateInput(string input, DataType type = DataType.NONE)
            {
                bool success = false;
                switch (type)
                {
                    case DataType.INT:
                        success = int.TryParse(input, out int numInt); break;
                    case DataType.DOUBLE:
                        success = double.TryParse(input, out double numDouble); break;
                    case DataType.FLOAT:
                        success = float.TryParse(input, out float numFloat); break;
                    default:
                        Console.WriteLine("Number type not matches."); break;
                }
                if (!success)
                    Console.WriteLine($"Attempted conversion of '{input ?? "<null>"}' to " + type + " failed.");
                return success;
            }

            public static bool ValidateInput(int num, int min, int max)
            {
                bool success = true;
                if (num < min || num > max)
                {
                    success = false;
                    Console.WriteLine("Number ({0}) out of range, must be between {1} and {2} ", num, min, max);
                }
                return success;
            }
            public  int GetMax(int first, int second)
            {
                int result=first > second ? first : second;
                /*if (first > second) return first;
                else  return second;*/
                return result;
            }
        }
    }
    namespace Arrays
    {
        class _Array
        {
            public void Return_Index(int[] array,int num, out int? index)
            {
                index = null;
                for (int i = 0; i < array.Length; i++)
                {
                    if (array[i] == num)
                    {
                        index= i;
                        break;
                    }
                }
               
            }
            public void PrintArray(int[] array, bool withIndex = false)
            {
                String _elements = "";
                foreach (int a in array)
                {
                    if (withIndex is true)
                        _elements += ("{" + (Array.IndexOf(array, a)).ToString() + "}=" + a + " ");
                    else
                        _elements += a + " ";
                }
                Console.WriteLine("[ " + _elements + "]");
            }
            public int[] AddArray()
            {
                int[] array = new int[0];
                Console.WriteLine("Please enter array length: ");
                String input = Console.ReadLine();
                if (Utilities.validators.ValidateInput(input, Utilities.DataType.INT))
                {
                    array = new int[Int32.Parse(input)];
                    for (int i = 0; i < array.Length; i++)
                    {
                        do
                        {
                            Console.Write("Please enter index[{0}] : ", i);
                            input = Console.ReadLine();
                        } while (!Utilities.validators.ValidateInput(input, Utilities.DataType.INT));
                        array[i] = Int32.Parse(input);
                    }
                }
                return array;
            }
            public void Swap(ref int[] array,out int biggest, int i)
            {
                biggest = array[i + 1];
                if (array[i] > array[i+1])
                {
                    array[i + 1] = array[i];
                    array[i] = biggest;
                    biggest = array[i];
                }
            }

        }
    }
    namespace Index_Value_To_Find
    {
        class Program
        {
            public static void Question1()
            {
                /*Write a method to find a value in an array.The method returns the index of the 
                 * searched value in the array. Manage the case when the given value is not found*/
                Arrays._Array objFindNum = new Arrays._Array();
                int[] array = objFindNum.AddArray();
                if (array.Length > 0)
                {
                    objFindNum.PrintArray(array);
                    Console.Write("Please enter number to find: ");
                    int input = Int32.Parse(Console.ReadLine());
                    int? index;
                    objFindNum.Return_Index(array, input, out index);
                    if (index is null)
                        Console.WriteLine("Not found value in the array");
                    else
                        Console.WriteLine("The index of the value {0}, is:{1}", input, index);
                }
            }
        }
    }
    namespace GetMax_Three_Numbers
    {
        class Program
        {
            public static void Question1()
            {
                /*1-Create a method GetMax() with two integer (int) parameters, that returns maximal of the two numbers. 
                 * Write a program that reads three numbers from the console and prints the biggest of them. Use the GetMax()
                 * method you just created. Write a test program that validates that the methods works correctly.*/
                try
                {
                    Dictionary<string, int> n = new Dictionary<string, int>();
                    for (int i = 1; i < 4; i++)
                    {
                        Console.WriteLine("Please enter {0} number: ", i);
                        int input = Int32.Parse(Console.ReadLine());
                        n.Add(String.Format("n_{0}", i.ToString()), input);
                    }
                    Utilities.validators ob = new Utilities.validators();
                    if ((n["n_1"] == n["n_2"]) && ( n["n_2"]== n["n_3"]))
                    {
                        int maxNum = ob.GetMax((ob.GetMax(n["n_1"], n["n_2"])), n["n_3"]);
                        Console.WriteLine("The biggest number beetween {0}, {1} and {2} is: {3}", n["n_1"], n["n_2"], n["n_3"], maxNum);
                    }
                    else
                    {
                        Console.WriteLine("The numbers  {0}, {1} and {2} and {3} Are the same", n["n_1"], n["n_2"], n["n_3"]);
                    }                 
                    /*foreach (var item in numbers)
                    { Console.Write(" key "+item.Key); Console.Write(" value "+item.Value);}*/
                }
                catch (Exception e)
                {
                    Console.WriteLine("Something went wrong. Error:" + e);
                }
            }
        }
    }
    namespace English_Name_Last_Digit
    {
        class Program
        {
            static void GetNameEnglish(string number)
            {
                switch (number[number.Length - 1])
                {
                    case '1':
                        Console.WriteLine("One"); break;
                    case '2':
                        Console.WriteLine("Two"); break;
                    case '3':
                        Console.WriteLine("Three"); break;
                    case '4':
                        Console.WriteLine("Four"); break;
                    case '5':
                        Console.WriteLine("Five"); break;
                    case '6':
                        Console.WriteLine("Six"); break;
                    case '7':
                        Console.WriteLine("Seven"); break;
                    case '8':
                        Console.WriteLine("Eight"); break;
                    case '9':
                        Console.WriteLine("Nine"); break;
                    case '0':
                        Console.WriteLine("Zero"); break;
                    default:
                        Console.WriteLine("Not inputted number."); break;
                }
            }
            public static void Question1()
            {
                /*2-Write a method that returns the English name of the last digit of a given number. 
                 * Example: for 512 prints "two"; for 1024 to "four".*/
                try
                {
                    Console.Write("Please Enter a number: ");
                    String input = Console.ReadLine();
                    if ((Utilities.validators.ValidateInput(input, Utilities.DataType.DOUBLE))
                        || (Utilities.validators.ValidateInput(input, Utilities.DataType.INT))
                        || (Utilities.validators.ValidateInput(input, Utilities.DataType.FLOAT)))
                    {
                        GetNameEnglish(input);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Something went wrong. Error:" + e);
                }
            }
        }
    }
    namespace Find_Number_Into_Matrix
    {
        class Program
        {
            static int CountNumberArray(int[] array, int number)
            {
                int counter = 0;
                for (int i = 0; i < array.Length; i++)
                {
                    if (number == array[i]) counter++;
                }
                return counter;
            }
            public static void Question1()
            {
                /*3-Write a method that finds how many times certain number can be found in a given array. 
                 * Write a program to test that the method works correctly.*/
                try
                {
                    Arrays._Array objFindNum = new Arrays._Array();
                    int[] array = objFindNum.AddArray();
                    if (array.Length > 0)
                    {
                        Console.Write("Please enter number to find: ");
                        String input = Console.ReadLine();
                        if (Utilities.validators.ValidateInput(input, Utilities.DataType.INT))
                        {
                            int num_Find = Int32.Parse(input);
                            Console.WriteLine("{0} was found {1} times.", num_Find, CountNumberArray(array, num_Find));
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Something went wrong. Error:" + e);
                }
            }

        }
    }
    namespace Checks_Elements_Arrays_Neighbors
    {
        class Program
        {
           
            static bool CheckElement_Position(int [] array, int value)
            {
                return ((array[value] > array[value + 1]) && (array[value] > array[value - 1]));
            }
            public static void Question1()
            {
                /*4-Write a method that checks whether an element, from a certain position in an array is greater
                 * than its two neighbors. Test whether the method works correctly..*/
                try
                {
                    Arrays._Array objCheckNum = new Arrays._Array();
                    int[] array = objCheckNum.AddArray();
                    if (array.Length > 0)
                    {
                        objCheckNum.PrintArray(array, true);
                        if (array.Length == 1 || array.Length == 2)
                            Console.WriteLine("The array only has only Lenght={0} element", array.Length);
                        else
                        {
                            Console.Write("Please enter index to validate: ");
                            String input = Console.ReadLine();
                            if (Utilities.validators.ValidateInput(input, Utilities.DataType.INT))
                            {
                                if (Utilities.validators.ValidateInput(Int32.Parse(input), 0, array.Length - 1))
                                {
                                    int index = Int32.Parse(input);
                                    if (CheckElement_Position(array,index))
                                        Console.WriteLine("{0} is bigger than it's two neighbors left:{1} and right:{2}"+
                                            "", array[index], array[index-1], array[index + 1]);
                                    else
                                        Console.WriteLine("{0} is not bigger than it's two neighbors left:{1} and right:{2}" +
                                           "", array[index], array[index - 1], array[index + 1]);
                                }
                                  
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Something went wrong. Error:" + e);
                }
            }

        }
    }
    namespace Find_Biggest_Element_Array
    {
        class Program
        {
            
            public static void Question1()
            {
                /*5-Write a method that finds the biggest element of an array. 
                 * Use that method to implement sorting in ascending order.*/
                try
                {
                    Arrays._Array objSortNum = new Arrays._Array();
                    int[] array = objSortNum.AddArray();
                    if (array.Length > 0)
                        { Console.Write("Array: "); objSortNum.PrintArray(array); }
                    if (array.Length == 1)
                        Console.WriteLine("The array only has one element, Lenght={0}", array.Length);
                    else
                    {
                        int biggest;
                        for (int j = 0; j <= array.Length - 2; j++)
                        {
                            for (int i = 0; i <= array.Length - 2; i++)
                            {
                                if (array[i] > array[i + 1])
                                {
                                    objSortNum.Swap(ref array,out biggest,i);
                                }
                            }
                        }
                        Console.Write("Sort:  "); objSortNum.PrintArray(array);
                        Console.WriteLine("{0} is the biggest element of the array.", array[array.Length - 1]);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine("Something went wrong. Error:" + e);
                }
            }
        }
    }
    namespace Min_Max_Array
    {
        class Program
        {
            public static void MinMaxArray(float[] data, ref float min, ref float max)
            {
                min = data[1];
                max = data[2];
            }
            public static void Question1()
            {
                /*6-Write a function named MinMaxArray, to return the minimum and maximum values stored in an array,
                 using reference parameters:
                float[] data = { 1.5f, 0.7f, 8.0f }  MinMaxArray(data, ref minimum, ref maximum);
                (after that call, minimum would contain 0.7, and maximum would contain 8.0)*/
                float[] data = { 1.5f, 0.7f, 8.0f };
                float minimum = data[0];
                float maximum = data[1];
                foreach (var item in data)
                    Console.WriteLine(" " + item.ToString());
                Console.WriteLine(" Variables before, Minimum= {0} and Maximum= {1}", minimum, maximum);
                MinMaxArray(data, ref minimum, ref maximum);
                Console.WriteLine(" Variables after, Minimum= {0} and Maximum= {1}", minimum, maximum);
            }
        }
    }
    namespace Calculate_Double_Integer_Number
    {
        class Calculator
        {
            public void Double(int n, out int m)
            {
                m = n * n;
            }
        }
        class Program
        {
            public static void Question1()
            {
                /*Write a method named "Double" to calculate the double of an integer number, and modify the data passed
                 * as an argument.It must be a "void" function and you must use the out parameter modifier.*/
                Console.WriteLine("Please enter integer number: ");
                String input = Console.ReadLine();
                if (Utilities.validators.ValidateInput(input, Utilities.DataType.INT))
                {
                    int numero = int.Parse(input);
                    new Calculator().Double(numero, out int m);
                    Console.WriteLine("Result: {0}", m);
                }
            }
        }
    }
    namespace Sum_All_Numbers
    {
        class Program
        {
            static int Add(params int[] nums)
            {
                int counter = 0;
                for (int i = 0; i < nums.Length; i++)
                {
                    Console.WriteLine(nums[i]);
                    counter += nums[i];
                }
                return counter;
            }
            public static void Question1()
            {
                /*Write a method named "Add" that will receives any number of integer parameters at run
                 * time and returns the sum of all those number. You should use the params modifier*/

                int[] array = new int[7] { 10, 20, 40,10,10,20,10 };
                Console.WriteLine("Sum of the total numbers: {0}", Add(array));
            }
        }
    }

    class Program
    {
        private static bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Choose an option:");
            Console.WriteLine("1) Returns the index of the value in array");
            Console.WriteLine("2) Exercise GetMax with three integer (int) parameters");
            Console.WriteLine("3) Exercise english name of the last digit of a given number");
            Console.WriteLine("4) Exercise how many times certain number found in a array");
            Console.WriteLine("5) Exercise checks elements neighbors of arrays");
            Console.WriteLine("6) Exercise finds the biggest element of an array");
            Console.WriteLine("7) Exercise minimum and maximum using reference parameters");
            Console.WriteLine("8) Exercise calculate the double of an integer number");
            Console.WriteLine("9) Exercise calculate sum of several numbers");
            Console.WriteLine("10) Exit");
            Console.Write("\r\nSelect an option: ");
            switch (Console.ReadLine())
            {
                case "1":
                    Console.Clear();
                    Index_Value_To_Find.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "2":
                    Console.Clear();
                    GetMax_Three_Numbers.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "3":
                    Console.Clear();
                    English_Name_Last_Digit.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "4":
                    Console.Clear();
                    Find_Number_Into_Matrix.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "5":
                    Console.Clear();
                    Checks_Elements_Arrays_Neighbors.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "6":
                    Console.Clear();
                    Find_Biggest_Element_Array.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "7":
                    Console.Clear();
                    Min_Max_Array.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "8":
                    Console.Clear();
                    Calculate_Double_Integer_Number.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "9":
                    Console.Clear();
                    Sum_All_Numbers.Program.Question1();
                    Console.ReadLine();
                    return true;
                case "10":
                    Console.WriteLine("Program end...");
                    Thread.Sleep(2000);
                    return false;
                default:
                    return true;
            }

        }
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            bool showMenu = true;
            while (showMenu)
            {
                showMenu = MainMenu();
            }
        }
    }
}
//Developed by Ricardo Andres Castano Bustos
