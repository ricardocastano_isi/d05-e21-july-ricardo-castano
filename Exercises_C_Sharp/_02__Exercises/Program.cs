﻿//Developed by Ricardo Andres Castano Bustos
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _02__Exercises
{
    namespace Types_and_variables
    {

        class Program
        {
            public static void Questions()
            {
                /*1-Declare several variables by selecting for each one of them the most appropriate 
                 of the types sbyte, byte, short, ushort, int, uint, long and ulong in order to assign
                 them the following values: 52,130; -115; 4825932; 97; -10000; 20000; 224; 970,700,000;
                 112; -44; -1,000,000; 1990; 123456789123456789.*/
                Console.Clear();
                Console.WriteLine(" Question 1:");
                float num1 = 52.130f;
                sbyte num2 = -115;
                uint num3 = 4825932;
                byte num4 = 97;
                short num5 = -10000;
                short num6 = 20000;
                double num7 = 970.700;
                sbyte num8 = 112;
                sbyte num9 = -44;
                double num10 = -1.000;
                ushort num11 = 1990;
                long num12 = 123456789123456789;
                Console.WriteLine("\tNum 1 (52,130) : {0}", num1 + " is " + Type.GetTypeCode(num1.GetType()));
                Console.WriteLine("\tNum 2 (-115) : {0}", num2 + " is " + Type.GetTypeCode(num2.GetType()));
                Console.WriteLine("\tNum 3 (4825932) : {0}", num3 + " is " + Type.GetTypeCode(num3.GetType()));
                Console.WriteLine("\tNum 4 (97) : {0}", num4 + " is " + Type.GetTypeCode(num4.GetType()));
                Console.WriteLine("\tNum 5 (-10000) : {0}", num5 + " is " + Type.GetTypeCode(num5.GetType()));
                Console.WriteLine("\tNum 6 (20000) : {0}", num6 + " is " + Type.GetTypeCode(num6.GetType()));
                Console.WriteLine("\tNum 7 (970.700) : {0}", num7 + " is " + Type.GetTypeCode(num7.GetType()));
                Console.WriteLine("\tNum 8 (112) : {0}", num8 + " is " + Type.GetTypeCode(num8.GetType()));
                Console.WriteLine("\tNum 9 (-44) : {0}", num9 + " is " + Type.GetTypeCode(num9.GetType()));
                Console.WriteLine("\tNum 10 (-1,000,000) : {0}", num10 + " is " + Type.GetTypeCode(num10.GetType()));
                Console.WriteLine("\tNum 11 (1990) : {0}", num11 + " is " + Type.GetTypeCode(num11.GetType()));
                Console.WriteLine("\tNum 12 (123456789123456789) : {0}", num12 + " is " + Type.GetTypeCode(num12.GetType())+"\n");

                //2-Declare a variable isMale of type bool and assign a value to it depending on your gender.
                Console.Write(" Question 2: ");
                bool isMale = true;
                if (isMale is true)
                {
                    Console.WriteLine("the gender is male \n");
                }

                /*3-Declare two variables of type string with values "Hello" and "World". Declare a variable 
                of type object. Assign the value obtained of concatenation of the two string variables 
                (add space if necessary) to this variable. Print the variable of type object.*/
                Console.Write(" Question 3: ");
                string string1 = "Hello";
                string string2 = "World";
                object obj = string1 + " " + string2;
                Console.WriteLine("Object is: {0}", obj.ToString());
                Console.ReadLine();
            }
        }
    }

    namespace Flow_control_statements
    {
        class Program
        {
            public static void Questions()
            {
                /*Write a program that applies bonus points to given scores in the range[1 to 9] by the following rules:
                If the score is between 1 and 3, the program multiplies it by 10.
                If the score is between 4 and 6, the program multiplies it by 100.
                If the score is between 7 and 9, the program multiplies it by 1000.
                If the score is 0 or more than 9, the program prints an error message*/
                Console.Clear();
                Console.WriteLine("Please enter integer between 1 and 9 :");
                int a = int.Parse(Console.ReadLine());

                switch (a)
                {
                    case 1:
                    case 2:
                    case (3):
                        Console.WriteLine("The bonus score is: {0}", (a * 10));
                        break;
                    case 4:
                    case 5:
                    case (6):
                        Console.WriteLine("The bonus score is: {0}", (a * 100));
                        break;
                    case 7:
                    case 8:
                    case 9:
                        Console.WriteLine("The bonus score is: {0}", (a * 1000));
                        break;
                    default:
                        Console.WriteLine("Invalid score");
                        break;
                }
                Console.ReadLine();

            }
        }
    }

    namespace Enumerations
    {
        /*Define a class Chat, which contains the following information about chats: username, usergender,
        * userage, e-mail and a chat status. Add an enumeration ChatStatus, which contains the values for
        * chat status (ONLINE, OFFLINE, BUSY, …) and use it as the type of field chatStatus for the class Chat*/

        enum status { ONLINE, OFFLINE, BUSY };
        class Program
        {
            class Chat
            {
                public string username;
                public string usergender;
                public byte userage;
                public string email;
                public status ChatStatus;

                public Chat(string username, string usergender, byte userage, string email, status ChatStatus)
                {
                    this.username = username;
                    this.usergender = usergender;
                    this.userage = userage;
                    this.email = email;
                    this.ChatStatus = ChatStatus;
                }
            }
            public static void Questions()
            {
                Console.Clear();
                Chat Obj = new Chat("Root", "Male", 30, "Richard@gmail.com", status.ONLINE);
                Console.WriteLine("Data:\n User name: {0}\n User gender: {1}\n User age: {2}\n" +
                    " Email: {3}\n Status: {4}\n", Obj.username, Obj.usergender, Obj.userage, Obj.email, Obj.ChatStatus);
                Console.ReadLine();
            }
        }
    }
    namespace Arrays
    {
        class Program
        {
            public static void PrintArray(int[] array, bool vertical)
            {
                foreach (int a in array)
                {
                    if (vertical is true)
                    {
                        Console.WriteLine("index_" + Array.IndexOf(array, a) + " = " + a + " ");
                    }
                    else
                    {
                        Console.Write(a + " ");
                    }
                }
                Console.WriteLine();
            }

            public static int[] ReadArray()
            {
                Console.Write("Please enter length of the array: ");
                int length = int.Parse(Console.ReadLine());
                int[] array = new int[length];
                for (int i = 0; i < length; i++)
                {
                    Console.Write("Please enter number of the array position: " + i + ": ");
                    array[i] = int.Parse(Console.ReadLine());
                }
                return array;
            }

            public static Boolean equalsArrays(int[] array1,int[] array2)
            {
                bool validation = true;
                if (array1.Length == array2.Length)
                {
                    for (int i = 0; i < array1.Length; i++)
                    {
                        if ((array1[i] - array2[i])!= 0){
                            validation = false;
                            break;
                        }
                    }
                }
                return validation;
            }

            public static  int[] Swap(int[] array,bool ascending)
            {
                int tmp;
                for (int j = 0; j <= array.Length - 2; j++)
                {
                    for (int i = 0; i <= array.Length - 2; i++)
                    {
                        if (ascending is true)
                        { 
                            if (array[i] > array[i + 1])
                            {
                                tmp = array[i + 1];
                                array[i + 1] = array[i];
                                array[i] = tmp;
                            }
                        }
                        else
                        {
                            if (array[i] < array[i + 1])
                            {
                                tmp = array[i];
                                array[i] = array[i+1];
                                array[i+1] = tmp;
                            }

                        }
                    }
                }
                return array;
            }
            public static void Questions1()
            {
                /*1-Write a program, which creates an array of 20 elements of type integer and initializes each of
                 * the elements with a value equals to the index of the element multiplied by 5.Print the elements to the console.*/
                Console.Clear();
                Console.WriteLine("Exercise 1 Arrays ");
                int[] array = new int[20];
                for (int i = 0; i < 20; i++)
                {
                    array[i]=i*5;
                }
                PrintArray(array,true);
                Console.ReadLine();

            }
            public static void Questions2()
            {
                /*Write a method, which reads two arrays from the console and checks whether they are equal 
                *(two arrays are equal when they are of equal length and all of their elements, which have the same index, are equal).*/
                Console.Clear();
                int[]  array1 = ReadArray();
                int[]  array2 = ReadArray();
                Console.Write("Array one: "); PrintArray(array1, false);
                Console.Write("Array two: "); PrintArray(array2, false);
                //if (array1.SequenceEqual(array2) is true)
                if ((equalsArrays(array1, array2)) is true)
                {
                    Console.WriteLine("The two arrays are equal");
                }
                else
                {
                    Console.WriteLine("The two arrays are not equal");
                }
                Console.ReadLine();
            }
            public static void Questions3()
            {
                /*Sorting an array means to arrange its elements in an increasing (or decreasing) order.
                 * Write a method, which sorts an array using the algorithm "bubble sort".*/
                int[] array = { 15, 4, 27, 100, 9, 1, 7, 77, 8 };
                Console.Clear();
                Console.Write("Array without sort: "); PrintArray(array, false);
                Console.WriteLine("Choose an option:");
                Console.WriteLine("1) Sort elements increasing");
                Console.WriteLine("2) Sort elements decreasing");
                Console.WriteLine("3) Exit (Default increasing)");
                Console.Write("\r\nSelect an option: ");

                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("Sort increasing");
                        array = Swap(array,true);
                        break;
                    case "2":
                        Console.WriteLine("Sort decreasing");
                        array = Swap(array, false);
                        break;
                    case "3":
                        Console.WriteLine("Sort increasing");
                        array = Swap(array, true);
                        break;
                    default:
                        Console.WriteLine("Program End...");
                        Console.WriteLine("Array not sort");
                        break;
                }
                
                Console.Write("Array: "); PrintArray(array, false);
                Console.ReadLine();
            }
        }
    }

    class Program
    {
        private static bool MainMenu()
        {
            Console.Clear();        
            Console.WriteLine("Choose an option:");
            Console.WriteLine("1) Exercise Types and variables");
            Console.WriteLine("2) Exercise Flow control statements");
            Console.WriteLine("3) Exercise Enumerations");
            Console.WriteLine("4) Exercise array of 20 elements * 5");
            Console.WriteLine("5) Exercise check two arrays are equal");
            Console.WriteLine("6) Exercise sorts array using 'bubble sort'");
            Console.WriteLine("7) Exit");
            Console.Write("\r\nSelect an option: ");
            switch (Console.ReadLine())
            {
                case "1":
                    Types_and_variables.Program.Questions();
                    return true;
                case "2":
                    Flow_control_statements.Program.Questions();
                    return true;
                case "3":
                    Enumerations.Program.Questions();
                    return true;
                case "4":
                    Arrays.Program.Questions1();
                    return true;
                case "5":
                    Arrays.Program.Questions2();
                    return true;
                case "6":
                    Arrays.Program.Questions3();
                    return true;
                case "7":
                    Console.WriteLine("Program end...");
                    Thread.Sleep(2000);
                    return false;
                default:
                    return true;
            }

        }
  
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            bool showMenu = true;
            while (showMenu)
            {
                showMenu = MainMenu();
            }
        }
    }
}
//Developed by Ricardo Andres Castano Bustos