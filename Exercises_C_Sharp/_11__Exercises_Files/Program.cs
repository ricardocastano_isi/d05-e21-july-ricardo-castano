﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace _11__Exercises_Files
{
    public class EncryptFile
    {
        public static string abc = "abcdefghijklmñnopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ1234567890";
        public static void WriteFile(string filePath)
        {
            File.WriteAllText(filePath, "this is an example of a message to encrypt");
        }

        public static void Encrypted(string inputFilePath, int caesarShift, string outputFilePath)
        {
            if (File.Exists(inputFilePath))
            {
                Console.WriteLine("Encrypting file...");
                Thread.Sleep(2000);
                string allTextlines = File.ReadAllText(inputFilePath);
                char[] letter = allTextlines.ToCharArray();
                String encryptMs = "";
                foreach (char l in letter)
                {
                    int posLetter = GetPosChar(l);
                    if (posLetter != -1)
                    {
                        int pos = posLetter - caesarShift;
                        while (pos < 0)
                            pos = pos + abc.Length;
                        encryptMs += abc[pos];
                    }
                    else
                        encryptMs += l;
                }
                if (File.Exists(outputFilePath))
                    { File.Delete(outputFilePath); }
                File.WriteAllText(outputFilePath, encryptMs);
                Console.WriteLine("File Encrypted in the path {0}", Path.GetFullPath(outputFilePath) );
            }
            else
            {
                Console.WriteLine("file does not exist in the path");
            }
            
        }
        public static int GetPosChar(char c)
        {
            for (int i = 0; i < abc.Length; i++)
            {
                if (c == abc[i])
                     return i;
            }
            return -1;
        }
    }

    public class Concatenate
    {
        public static void WriteFile(string filePath,string[] msm)
        {
            if (File.Exists(filePath))
                { File.Delete(filePath); }
            File.WriteAllLines(filePath, msm);
                
        }
        
        public static void Concatenate_(string filePath1, string filePath2, string outputFilePath)
        {
            string[] lines1 = File.ReadAllLines(filePath1);
            string[] lines2 = File.ReadAllLines(filePath2);
            if (File.Exists(outputFilePath))
                { File.Delete(outputFilePath); }
            File.AppendAllLines(outputFilePath, lines1);
            File.AppendAllLines(outputFilePath, lines2);
            Console.WriteLine("File Concatened in the path {0}", Path.GetFullPath(outputFilePath));
        }

    }
    class Program
    {
       
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Exercises Encrypt");
                string filepath = "../../message.txt";
                EncryptFile.WriteFile(filepath);
                string fileEncrypted = "../../encrypted.txt";
                EncryptFile.Encrypted(filepath, 10, fileEncrypted);

                Console.WriteLine("\nExercises Concatenate FIles");
                string[] lines1 = { "qwertyuiop", "asdfghjkl", "zxcvbnm" };
                Concatenate.WriteFile("../../file1.txt", lines1);

                string[] lines2 = { "line 1 file 2", "line 2 file 2", "line 3 file 2" };
                Concatenate.WriteFile("../../file2.txt", lines2);
                Concatenate.Concatenate_("../../file1.txt", "../../file2.txt", "../../outputFile.txt");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error files: {0}", e);
            }
            Console.ReadLine();
        }
    }
}
