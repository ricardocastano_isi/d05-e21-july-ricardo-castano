﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _06__Exercises
{
    class Start
    {
        class Program
        {
            private static bool MainMenu()
            {
                Console.Clear();
                Console.WriteLine("Choose an option:");
                Console.WriteLine("1) Exercise Inheritance Student");
                Console.WriteLine("2) Exercise Inheritance Worker");
                Console.WriteLine("3) Exercise Polymorphism Animals");
                Console.WriteLine("4) Exercise Polymorphism Kitchen Appliance");
                Console.WriteLine("5) Exercise Abstract Employee");
                Console.WriteLine("6) Exercise Abstract StorageDevice");
                Console.WriteLine("7) Exercise Interface IConvertible");
                Console.WriteLine("8) Exercise Interface StorageDevice");
                Console.WriteLine("9) Exit");
                Console.Write("\r\nSelect an option: ");
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.Clear();
                        Student_ s = new Student_("Andres", "Castano", 35);
                        s.GetData();
                        Console.ReadLine();
                        return true;
                    case "2":
                        Console.Clear();
                        Worker w = new Worker("Camilo","Bustos",20,160);
                        w.GetData();
                        Console.ReadLine();
                        return true;
                    case "3":
                        Console.Clear();
                        TestAppAnimal t = new TestAppAnimal();
                        t.Testing();
                        Console.ReadLine();
                        return true;
                    case "4":
                        Console.Clear();
                        Person_ p = new Person_();
                        p.UseKitchenAppliance(new Stove());
                        Person_ p2 = new Person_();
                        p2.UseKitchenAppliance(new Microwave());
                        Console.ReadLine();
                        return true;
                    case "5":
                        Console.Clear();
                        TestEmployee e = new TestEmployee();
                        e.Print();
                        Console.ReadLine();
                        return true;
                    case "6":
                        Console.Clear();
                        TestStorageDevice te = new TestStorageDevice();
                        te.Print();
                        Console.ReadLine();
                        return true;
                    case "7":
                        Console.Clear();
                        TestingInterface t3 = new TestingInterface();
                        t3.Test();
                        Console.ReadLine();
                        return true;
                    case "8":
                        Console.Clear();
                        
                        Console.ReadLine();
                        return true;
                    case "9":
                        Console.WriteLine("Program end...");
                        Thread.Sleep(2000);
                        return false;
                    default:
                        return true;
                }

            }
            static void Main(string[] args)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                bool showMenu = true;
                while (showMenu)
                {
                    showMenu = MainMenu();
                }
            }

        }
    }
}
