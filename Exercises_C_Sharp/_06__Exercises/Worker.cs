﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _06__Exercises
{
    class Worker:Human
    {

        private double rate;
        private double hoursWorked;
        public double Rate
        {
            get
            { return rate; }
            set
            {
                if (value > 0)
                    this.rate = value;
                else
                    this.rate = 0.0;
            }
        }
        public double HoursWorked
        {
            get
            { return hoursWorked; }
            set
            {
                if (value > 0)
                    this.hoursWorked = value;
                else
                    this.hoursWorked = 0.0;
            }
        }
        public Worker(string firstName, string lastName, double rate, double workHours)
        {
            FirstName = firstName;
            LastName = lastName;
            this.Rate = rate;
            this.HoursWorked = workHours;
        }

        public override void GetData()
        {
            base.GetData();
            Console.WriteLine("Rate: {0}, Hours Worked= {1}, Worker's salary is: {2}", Rate, HoursWorked,Rate*HoursWorked);
        }

        public void CalculateSalary()
        {
            Console.Write(" Human--> First name: {0}, Last name: {1}\n", FirstName, LastName);
        }
        ~Worker()
        {
            Console.WriteLine("destroying Worker object...");
            Thread.Sleep(2000);
        }
    }
    
    public class Human
    {
        public String FirstName { get; set; }
        public String LastName { get; set; }
       
        public virtual void GetData()
        {
            Console.Write("Class Human--> First name: {0}, Last name: {1}\n", FirstName, LastName);
        }
        ~Human()
        {
            Console.WriteLine("destroying Human object...");
            Thread.Sleep(2000);
        }
    }
    class Student_:Human
    {
        public int Mark { get; set; }
        public Student_(string firstName, string lastName,int mark)
        {
            FirstName = firstName;
            LastName = lastName;
            this.Mark = mark;
        }
        
        public override void GetData()
        {
            base.GetData();
            Console.WriteLine("Mark: {0}\n",Mark);
        }
        ~Student_()
        {
            Console.WriteLine("destroying Student object...");
            Thread.Sleep(2000);
        }

    }   
}
