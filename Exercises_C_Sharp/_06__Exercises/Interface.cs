﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06__Exercises
{
    interface IConvertible
    {
        String ConvertToCSharp(string stringToConvert);
        String ConvertToVB(string stringToConvert);
    }
    interface ICodeChecker:IConvertible
    {
        bool CodeCheckSyntax(string stringToCheck, string languageToUse);
    }
    public class ProgramHelper : IConvertible
    {
        public String ConvertToCSharp(String stringToConvert)
        {
            Console.WriteLine("Converting ''{0}'' to CSharp", stringToConvert);
            return "CSharp convert the string";
        }
        public String ConvertToVB(string stringToConvert)
        {
            Console.WriteLine("Converting ''{0}'' to VB", stringToConvert);
            return "VB convert the string";
        }
        public bool CodeCheckSyntax(string stringToCheck, string languageToUse)
        {
            switch (languageToUse)
            {
                case "CSharp":
                    Console.WriteLine("The string ''{0}'' to convert CSharp", stringToCheck);
                    return true;
                case "VB":
                    Console.WriteLine("The string ''{0}'' to convert VB", stringToCheck);
                    return true;
                default:
                    return false;
            }
        }
    }
    class TestingInterface
    {
        public void Test()
        {
            ProgramHelper c = new ProgramHelper();
            Console.WriteLine(c.ConvertToCSharp("Sintax Testing CSharp"));
            Console.WriteLine(c.ConvertToVB("Sintax Testing VB"));
            Console.WriteLine("------------------------------------------------");
            c.CodeCheckSyntax("Console.WriteLine", "CSharp");
            c.CodeCheckSyntax("Count.Count", "VB");
        }
    }
}
