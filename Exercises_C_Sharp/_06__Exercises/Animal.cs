﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06__Exercises
{
    class Animal
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }

        public void Animals(string name, int age, String gender)
        {
            this.Name = name;
            this.Age = age;
            this.Gender = gender;
        }
        public virtual void Sound()
        {
            Console.WriteLine("Sounds Animals: ");
        }
    }

    class Dog : Animal
    {
        public override void Sound()
        {
            base.Sound();
            Console.WriteLine("The dog {0} makes  Guaoo, is {1} years old and it's gender is {2}", Name,Age,Gender);
        }
    }
    class Frog : Animal
    {
        public override void Sound()
        {
            base.Sound();
            Console.WriteLine("The frog {0} makes  Miauuu, is {1} years old and it's gender is {2}", Name, Age, Gender);
        }
    }
    class Cat : Animal
    {
        public override void Sound()
        {
            base.Sound();
            Console.WriteLine("The cat {0} makes  Miauuu cat , is {1} years old and it's gender is {2}", Name, Age, Gender);
        }
    }
    class Kitten : Animal
    {
        public override void Sound()
        {
            base.Sound();
            Console.WriteLine("The kitten {0} makes  Miauuu kitten, is {1} years old and it's gender is {2}", Name, Age, Gender);
        }
    }
    class Tomcat : Animal
    {
        public override void Sound()
        {
            base.Sound();
            Console.WriteLine("The tomcat {0} makes  miauuu tomcat, is {1} years old and it's gender is {2}", Name, Age, Gender);
        }
    }
    class TestAppAnimal
    {
        public void Testing()
        {
            Animal[] Animal = new Animal[5];
            Animal[0] = new Dog();
            Animal[0].Animals("cokito",10,"Female");
            Animal[1] = new Frog();
            Animal[1].Animals("Trosqui", 2, "Male");
            Animal[2] = new Cat();
            Animal[2].Animals("Roky", 4, "Female");
            Animal[3] = new Kitten();
            Animal[3].Animals("Pirn", 7, "Male");
            Animal[4] = new Tomcat();
            Animal[4].Animals("other", 1, "Male");
            PrintAnimals(Animal);
        }
        public void PrintAnimals(Animal[] animals)
        {
            for (int i = 0; i < animals.Length; i++)
            {
                animals[i].Sound();
            }
            
        }
    }

}
