﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06__Exercises
{
    class KitchenAppliance
    {
        public String Brand { get; set; }
        public DateTime ManufacturingDate { get; set; }

        public virtual void PowerOn()
        {
            Console.WriteLine("KitchenAppliance Power ON");
        }
    }
    class Stove: KitchenAppliance
    {
        public override void PowerOn()
        {
            Console.WriteLine("Stove Power ON");
        }
    }
    class Microwave: KitchenAppliance
    {
        public override void PowerOn()
        {
            Console.WriteLine("Microwave Power ON");
        }
    }
    class Person_
    {
        public void UseKitchenAppliance(KitchenAppliance ka)
        {
            ka.PowerOn();
            Console.WriteLine("Person in Kitchen...");
            
        }
    }
}
