﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _06__Exercises
{
    public class School
    {
        public String SchoolName { get; set; }
        public List<Class_> classes;
    }
    public class Person
    {
        public String FirstName { get; set; }
        public String LastName { get; set; }
    }

    public class Student:Person
    {
        public int NumStudent { get; set; }
    }


    public class Teacher:Person
    {
        public int NumTeacher { get; set; }
    }

    public class Course
    {
        public String NameCourse { get; set; }
        public int CountClasses { get; set; }
        public int CountExercises { get; set; }
    }

    public class Class_
    {
        private String Identifier { get; set; }
        public List<Student> students;
    }

    
}
