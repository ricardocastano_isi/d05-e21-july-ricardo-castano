﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06__Exercises
{
    abstract class Employee
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public double YearlySalary { get; set; }

        public double GetMonthlySalary(double salary,int months)
        {
            return salary / months;
        }

        public abstract void PrintData();
       
    }

    class Subclasses1:Employee
    {
        public override void PrintData()
        {
            Console.WriteLine("SubClass1\nName: {0}\nAddress: {1}\nMontly Salary: {2}\n",Name,Address,this.GetMonthlySalary(YearlySalary,12) );
        }
    }
    class Subclasses2 : Subclasses1
    {
        public override void PrintData()
        {
            Console.WriteLine("SubClass2\nName: {0}\nAddress {1}\nMontly Salary: {2}\n", Name, Address,this.GetMonthlySalary(YearlySalary,10));
        }
    }
    class TestEmployee
    {
        public void Print()
        {
            Subclasses1 s1 = new Subclasses1 { Name = "Andres Castano", Address = "Street 123", YearlySalary = 60000 };
            s1.PrintData();
            Subclasses1 s2 = new Subclasses1 { Name = "otro", Address = "Abcd 8", YearlySalary = 2000 };
            Employee e2 = s2;
            e2.PrintData();
            Employee e3 = new Subclasses2 { Name = "Felipe Bustos", Address = "Rue Adam 4", YearlySalary = 120000 };
            e3.PrintData();
        }
    }
    abstract class StorageDevice
    {
        public abstract void Read();
        public abstract void Write();

    }
    class USBDisk:StorageDevice
    {
        public override void Read()
        {
            Console.WriteLine("Usb Disk Read");
        }
        public override void Write()
        {
            Console.WriteLine("Usb Disk Write");
        }

    }
    class SmartPhone:StorageDevice
    {
        public override void Read()
        {
            Console.WriteLine("SmartPhone Read");
        }
        public override void Write()
        {
            Console.WriteLine("SmartPhone Write");
        }
    }
    class Computer
    {
        public void ReadData(StorageDevice device)
        {
            Console.WriteLine("Computer Read Data:");
            device.Read();
        }
        public void WriteData(StorageDevice device)
        {
            Console.WriteLine("Computer Write Data:");
            device.Write();
        }
    }
    public class TestStorageDevice
    {
        public void Print()
        {
            USBDisk u = new USBDisk();
            SmartPhone s = new SmartPhone();
            Computer c = new Computer();
            c.ReadData(u);
            c.WriteData(u);
            c.ReadData(s);
            c.WriteData(s);
        }
    }

}
