﻿//Developed by Ricardo Andres Castano Bustos
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01__Exercises
{
    namespace Question1
    {
        class Program
        {
            public static void Question1()
            {
                Console.WriteLine("Circle App");
                Console.Write("Please enter radius of a circle: ");
                double radius = double.Parse(Console.ReadLine());
                double pi = 3.141516;
                double perimeter, area;
                if (radius > 0)
                {
                    perimeter = (pi * 2) * radius;
                    area = pi * (radius * radius);
                    Console.WriteLine("The perimeter is  {0}", perimeter);
                    Console.WriteLine("The area is {0}" , area);
                }
                else
                {
                    Console.WriteLine("Nothing entered");
                }
                Console.ReadLine();

            }
        }
    }

    namespace Question2
    {
        class Program
        {
            public static void Question2()
            {
                Console.Write("Please enter your company name: ");
                string name = Console.ReadLine();
                Console.Write("Please enter the address: ");
                string address = Console.ReadLine();
                Console.WriteLine("Please enter a company phone number");
                string companyphoneNumber = Console.ReadLine();
                Console.WriteLine("Please enter a company fax number");
                string companyfaxNumber = Console.ReadLine();
                Console.WriteLine("Please enter a company website");
                string website = Console.ReadLine();
                Console.WriteLine("Please enter a manager name");
                string managerName = Console.ReadLine();
                Console.WriteLine("Please enter a manager surname");
                string managersurname = Console.ReadLine();
                Console.WriteLine("Please enter a manager phone number");
                string managerPhoneNumber = Console.ReadLine();
                Console.WriteLine("Name company:{0}, Address:{1}, Phone number:{2}, Fax number:{3}, Website:{4}," +
                    " Manager name:{5}, Manager surname:{6}, Manager phone number:{7}"
                    , name, address, companyphoneNumber, companyfaxNumber, website, managerName, managersurname, managerPhoneNumber);
                Console.ReadLine();
            }
        }
    }

    namespace Question3
    {
        class Program
        {
            public static void Question3()
            {
                int n,num,acum=0;
                Console.Write("Please enter your integer number: ");
                n = Convert.ToInt32(Console.ReadLine());
                for (int i = 1; i <= n;i++)
                {
                    Console.Write("Please enter your integer number {0}: ",i);
                    num = Convert.ToInt32(Console.ReadLine());
                    if (num > 0)
                    {
                        acum += num;
                    }
                }
                Console.WriteLine("Sum of "+n+" numbers is :{0} ", acum);
                Console.ReadLine();
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Question1.Program.Question1();
            Question2.Program.Question2();
            Question3.Program.Question3();

        }
    }
}
//Developed by Ricardo Andres Castano Bustos