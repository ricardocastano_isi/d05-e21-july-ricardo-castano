﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07__Exercises
{
    namespace DGSaySomething
    {
        delegate void DGSaySomething(string str);
            class Person
            {
                public virtual void SaySomething(string aString)
                {
                    Console.WriteLine("The Person Say {0}: ", aString);
                }
            }
            class Chinese : Person
            {
                public override void SaySomething(string aString)
                {
                    Console.WriteLine("The Chinese say {0}", aString);
                }
            }
            public class Program
            {
                public static void Test()
                {
                    Person p = new Person();
                    Person c = new Chinese();
                    p.SaySomething("Hellooo");
                    DGSaySomething Dgs = new DGSaySomething(p.SaySomething);
                    Dgs += c.SaySomething;
                    Dgs.Invoke("Bonjour");
                    
                }
            }
    }
    namespace DoorOpen
    {
        delegate void DGDoor();
        class Door
        {
            public int Id { get; set; }
            public bool IsOpen { get; set; }
            public DGDoor doorOn;

            public void DoorOn()
            {
                this.doorOn?.Invoke();
            }

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Exercise Say Something: ");
            DGSaySomething.Program.Test();
            Console.WriteLine("Exercise Door: ");
            Console.ReadLine();
            
       
        }
    }
}
