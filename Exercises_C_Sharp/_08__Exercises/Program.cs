﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08__Exercises
{
    namespace ArrayList_
    {
        class Person
        {
            public String Name { get; set; }
            public int Age { get; set; }
            public Person(string name, int age)

            {
                Name = name;
                Age = age;
            }
        }
        class Student : Person
        {
            public readonly string StuNumber;
            public Student(string stuNumber, string name, int age)
                : base(name, age)
            {
                StuNumber = stuNumber;
            }
        }
        class Employee : Person
        {
            public float Salary { set; get; }
            public Employee(string name, int age, float salary)
                : base(name, age)
            {
                Salary = salary;
            }
        }
        public class Test
        {
            public static void Testing()
            {
                ArrayList aL = new ArrayList();
                aL.Add(new Student("00457", "Andres Castano ", 35));
                aL.Add(new Student("00457", "Maria Pacher", 25));
                aL.Add(new Employee("Juan Perez ", 54, 10000));
                aL.Add(new Employee("Catalina la o", 21, 4500));            
                Print(aL);
            }

            public static void Print(ArrayList arr)
            {
                foreach (var item in arr)
                {
                    if (item is Student)
                        Console.WriteLine(" Data Student\n IDStudent: {0}\n Name: {1}\n age: {2}\n"+
                            "", ((Student)item).StuNumber, ((Student)item).Name, ((Student)item).Age);
                    else
                        Console.WriteLine(" Data Employeed\n Name: {0}\n age: {1}\n salary: {2}\n"+
                            "", ((Employee)item).Name, ((Employee)item).Age, ((Employee)item).Salary);
                }
            }
        }
    }
    namespace List
    {
        public class Prime
        {
            static List<int> GetPrimes(int start, int end)
            {
                List<int> primes = new List<int>();
                bool isPrime = true;
                for (int i = start; i <= end; i++)
                {
                    for (int j = 2; j <= end; j++)
                    {
                        if (i != j && i % j == 0)
                        {
                            isPrime = false;
                            break;
                        }
                    }
                    if (isPrime)
                    {
                        primes.Add(i);
                    }
                    isPrime = true;
                }
                return primes;
            }
            public static void Print()
            {
                foreach (int prime in GetPrimes(2, 100))
                {
                    Console.Write(prime+" ");
                }
            }
        }
    }

    namespace Dictionary
    {
        class Person
        {
            public string Name { get; set; }

            public override string ToString()
            {
                return this.Name;
            }
        }

        class BankAccount
        {
            public int AccountNumber { get; set; }
            public double Amount { get; set; }

            public override string ToString()
            {
                return string.Format("{0} ({1} $)", this.AccountNumber, this.Amount);
            }
        }
        public class Test
        {
            public static void Testing()
            {
                Person p1 = new Person { Name = "Andres" };
                Person p2 = new Person { Name = "Maria" };
                Person p3 = new Person { Name = "Felipe" };

                BankAccount b1 = new BankAccount { AccountNumber = 1000, Amount = 10000 };
                BankAccount b2 = new BankAccount { AccountNumber = 10001, Amount = 4000 };
                BankAccount b3 = new BankAccount { AccountNumber = 10002, Amount = 7000 };

                Dictionary<Person, BankAccount> dic = new Dictionary<Person, BankAccount>();

                dic.Add(p1,b1);
                dic.Add(p2, b2);
                dic.Add(p3, b3);

                Console.WriteLine("Dictionary 1:");
                foreach (var item in dic)
                {                
                    Console.WriteLine("Name: {0}, Data Account: {1}",item.Key, item.Value);
                }
                // Dictionary 2
                List < BankAccount > list=new List<BankAccount> { b1, b2 };
                List<BankAccount> list2 = new List<BankAccount> { b2, b3 };
                Dictionary<Person, List<BankAccount>> dic2 = new Dictionary<Person, List<BankAccount>>();
                dic2.Add(p1,list );
                dic2.Add(p2,list2 );
                Console.WriteLine("\nDictionary 2:");
                foreach (var item in dic2)
                {
                    Console.WriteLine("Key: ({0})", item.Key);
                    foreach (var bankAccount in item.Value)
                    {
                        Console.WriteLine("Value({0})", bankAccount);
                    }
                }
                // Dictionary 3
                Dictionary<Person, Dictionary<int, BankAccount>> dic3 = new Dictionary<Person, Dictionary<int, BankAccount>>();
                Dictionary<int, BankAccount> dicAndres = new Dictionary<int, BankAccount>();
                dicAndres.Add(1, b1);
                dicAndres.Add(2, b2);
                Dictionary<int, BankAccount> dicMaria = new Dictionary<int, BankAccount>();
                dicMaria.Add(1,b2);
                dicMaria.Add(2,b3);
                dic3.Add(p1, dicAndres);
                dic3.Add(p2, dicMaria);
                Console.WriteLine("\nDictionary 3:");
                foreach (var item in dic3)
                {
                    Console.WriteLine("Key({0})", item.Key);
                    foreach (var bankAccount in item.Value)
                        Console.WriteLine("Value(Key({0}) and Value({1})", bankAccount.Key, bankAccount.Value);
                }
            }
        }


    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" Exercise List: ");
            List.Prime.Print();
            Console.WriteLine(" \nExercise Array List: ");
            ArrayList_.Test.Testing();
            Console.WriteLine(" \nExercise Dictionary: ");
            Dictionary.Test.Testing();
            Console.ReadLine();
        }
    }

}
